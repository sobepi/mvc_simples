<?php
include_once './../configuracao/Configuracao.php';

abstract class AbstractDao{
    
        protected $conexaoDao;
    
        public function __construct(){
            $this->conexaoDao = new Configuracao();  
            self::getConexao()->startConexao();
        }
        
    
        protected function getConexao()
        {
            return $this->conexaoDao;
        }
    
  
    
    
}