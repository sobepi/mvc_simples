<?php 


include_once 'ControllerSession.php';
include_once '../model/dao/FormularioDao.php';

class ControllerFormulario{
    
    public function verificarCampos($var1,$var2,$var3,$var4){
        if($var1 == NULL || $var2 == NULL || $var3 == NULL || $var4 == NULL){
            return false;
        }
        else{
            return true;
        }
    }
   
    public function executarLogin(Session $session){
        $formularioDao = new FormularioDao();
        $usuario = new Usuario();
        $pessoa = new Pessoa();
        
        if(self::verificarCampos($_GET['nome'], $_GET['email'], $_GET['senha'], $_GET['login'])){
            $session->set('nome', $_GET['nome']);
            $session->set('email',$_GET['email']);
            $session->set('senha',$_GET['senha']);
            $session->set('login',$_GET['login']);
               
            $pessoa->setNome($session->get('nome'));
            $pessoa->setEmail($session->get('email'));
            $usuario->setLogin($session->get('login'));
            $usuario->setSenha($session->get('senha'));
        
            $cadastro = $formularioDao->cadastrar($usuario,$pessoa);
            header('Location: ../view/termino.php');
            $session->destroy();
        }
        }

}



?>